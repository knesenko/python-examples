def balance_parentheses(string):
    list_string = [s for s in string]
    left_stack = []
    right_stack = []

    for index, char in enumerate(list_string):
        if char == "(":
            left_stack.append(index)
        elif char == ")":
            if not left_stack:
                right_stack.append(index)
            else:
                left_stack.pop()

    for i, index in enumerate(left_stack + right_stack):
        if i == 0:
            del list_string[index]
        else:
            del list_string[index-i]

    return "".join(list_string)


print(balance_parentheses("i7difdk(ds()))98ijdskjf(89lk)8w(((()))))"))
print(balance_parentheses("(()))()(((()))))"))
print(balance_parentheses("((a((aa(((()d(())("))


