Given a number, rearrange the digits of that number to make a higher number, among all such permutations that are greater,one of them is the smallest, Find the smallest greater permutation (the next Permutation).

Examples:
next_permutation (12) = 21
next_permutation (315) = 351
next_permutation (583) = 835
next_permutation (12389) = 12398
next_permutation (34722641) = 34724126