class Node(object):
    def __init__(self, value, left=None, right=None):
        self._left = left
        self._right = right
        self._value = value

    @property
    def left(self):
        return self._left

    @property
    def right(self):
        return self._right

    @property
    def value(self):
        return self._value

    @left.setter
    def left(self, value):
        if not isinstance(value, Node):
            raise TypeError("Left leaf should be from type: Node")
        self._left = value

    @right.setter
    def right(self, value):
        if not isinstance(value, Node):
            raise TypeError("Right leaf should be from type: Node")
        self._right = value

    @value.setter
    def value(self, value):
        if not isinstance(value, int):
            raise TypeError("Value should be from type: int")
        self._value = value


def get_max_path_sum(root_node):
    left_sum = 0
    right_sum = 0
    if not root_node.left and not root_node.right:
        return root_node.value

    if root_node.left:
        left_sum += root_node.value + get_max_path_sum(root_node.left)

    if root_node.right:
        right_sum += root_node.value + get_max_path_sum(root_node.right)

    return max(left_sum, right_sum)


root_node = Node(value=0)
first_node = Node(value=1)
second_node = Node(value=2)
third_node = Node(value=3)
fourth_node = Node(value=4)

root_node.left = first_node
root_node.right = second_node
second_node.right = third_node
first_node.left = fourth_node
first_node.right = Node(value=10)
third_node.right = Node(value=10)

print(get_max_path_sum(root_node=root_node))
