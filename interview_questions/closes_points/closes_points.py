from math import sqrt

points_list = [(3, 2), (1, 3), (4, 5), (2, 4), (2, 3)]

k = 3


def euclidean_distance(p):
    return sqrt(p[0] ** 2 + p[1] ** 2)


def partition(points, start_index, end_index):
    pos_index = start_index

    for i in range(start_index, end_index):
        if euclidean_distance(points[end_index]) > euclidean_distance(points[i]):
            points[pos_index], points[i] = points[i], points[pos_index]
            pos_index += 1

    points[pos_index], points[end_index] = points[end_index], points[pos_index]

    return pos_index


def quick_sort(points, start, end):
    if start < end:
        pivot = partition(points, start, end)
        quick_sort(points, start, pivot - 1)
        quick_sort(points, pivot + 1, end)


def knn(points, k):
    quick_sort(points, 0, len(points) - 1)
    return points[:k]


print('result:', knn(points_list, 3))
