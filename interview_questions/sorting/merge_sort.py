"""
https://www.youtube.com/watch?v=TzeBrDU-JaY&index=5&list=PL2_aWCzGMAwKedT2KfDMB9YA5DgASZb3U
"""

items = [2, 4, 1, 6, 8, 5, 3, 7]


def merge(left_list, right_list, center_list):
    left_list_length = len(left_list)
    right_list_length = len(right_list)

    left_pointer, right_pointer, center_pointer = 0, 0, 0
    while left_pointer < left_list_length and right_pointer < right_list_length:
        if left_list[left_pointer] <= right_list[right_pointer]:
            center_list[center_pointer] = left_list[left_pointer]
            left_pointer += 1
        else:
            center_list[center_pointer] = right_list[right_pointer]
            right_pointer += 1

        center_pointer += 1

    while left_pointer < left_list_length:
        center_list[center_pointer] = left_list[left_pointer]
        left_pointer += 1
        center_pointer += 1

    while right_pointer < right_list_length:
        center_list[center_pointer] = right_list[right_pointer]
        right_pointer += 1
        center_pointer += 1


def merge_sort(items_list):
    list_of_items_length = len(items_list)
    if list_of_items_length < 2:
        return

    mid = list_of_items_length // 2

    left_list = items_list[:mid]
    right_list = items_list[mid:]

    merge_sort(left_list)
    merge_sort(right_list)
    merge(left_list, right_list, items_list)


merge_sort(items)
print(items)
