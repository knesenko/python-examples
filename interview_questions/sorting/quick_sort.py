"""
https://www.youtube.com/watch?v=COk73cpQbFQ&index=7&list=PL2_aWCzGMAwKedT2KfDMB9YA5DgASZb3U
"""
numbers = [900, 901, 0, 117, 100]

print(numbers)


def partition(items, start, end):
    pivot = items[end]
    partition_index = start

    for i in range(start, end):
        if items[i] <= pivot:
            print("Switching {:d} and {:d}".format(items[i], items[partition_index]))
            items[i], items[partition_index] = items[partition_index], items[i]
            partition_index += 1

    items[end], items[partition_index] = items[partition_index], items[end]
    print(items)
    print(partition_index)

    return partition_index


def quick_sort(items, start, end):
    if start < end:
        partition_index = partition(items=items, start=start, end=end)
        quick_sort(items=items, start=start, end=partition_index-1)
        quick_sort(items=items, start=partition_index+1, end=end)

    return numbers


print(quick_sort(items=numbers, start=0, end=len(numbers) - 1))

