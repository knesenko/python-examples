
numbers = sorted([2, 3, 6, 15, 34, 07, 45, 31, 94, 23, 100])


def binary_search_iterative(items, searched_number):
    """
    https://www.youtube.com/watch?v=j5uXyPJ0Pew
    """
    start = 0
    end = len(items) - 1

    while start <= end:
        mid = (start + end) // 2
        if items[mid] == searched_number:
            return mid
        elif searched_number < items[mid]:
            end = mid - 1
        else:
            start = mid + 1

    return -1


def binary_search_recursive(items, low, high, searched_number):
    """
    https://www.youtube.com/watch?v=-bQ4UzUmWe8
    """
    if low > high:
        return -1

    mid = low + (high - low) // len(items)
    if searched_number == items[mid]:
        return mid

    elif searched_number < items[mid]:
        return binary_search_recursive(items=items, low=low, high=mid - 1, searched_number=searched_number)
    else:
        return binary_search_recursive(items=items, low=mid + 1, high=high, searched_number=searched_number)


print(numbers)
print(binary_search_iterative(items=sorted(numbers), searched_number=34))
print(binary_search_recursive(items=sorted(numbers), low=0, high=len(numbers) - 1, searched_number=34))
