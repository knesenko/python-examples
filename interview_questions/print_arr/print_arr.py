#!/usr/bin/env python


array = [1, 20, 300, 4000, 50000, 12345678]


def print_arr(arr, limit):
    buff = ""

    for i in arr:
        str_item = str(i)
        if len(buff) + len(str_item) + 1 > limit:
            if buff:
                print(buff)
            buff = ""

        buff += str_item + ","

    print(buff[:-1])


print_arr(array, 16)
