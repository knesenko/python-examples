class ParrentPatterns(object):
    def __init__(self, points):
        self._points = points

    @property
    def points(self):
        return self._points

    @points.setter
    def points(self, value):
        if not isinstance(value, dict):
            raise TypeError("points should be from type dict")
        self._points = value

    def resolve_childs(self, string):
        if not len(string):
            return {}

        print("observing {}".format(string))

        end_result = set()
        for i, c in enumerate(string):
            if string[0:i + 1] in self.points:
                s = string[0:i + 1]
                find_childs_result = self.resolve_childs(string[i + 1:])
                for x in self.points[s]:
                    if not len(find_childs_result):
                        print("No child found for {x} adding {x} to end_result".format(x=x))
                        end_result.add(x)
                    else:
                        for y in find_childs_result:
                            print("I am:{} and adding x+y:{} to end_result".format(x, x + y))
                            end_result.add(x + y)

                    print(end_result)

        return end_result


points = {
    '1': ['A', 'B', 'C'],
    '2': ['D', 'E'],
    '12': ['X'],
    '3': ['P', 'Q']
}

patterns = ParrentPatterns(points)
print(list(patterns.resolve_childs(points="12")))
