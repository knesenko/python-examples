class Graph(object):
    def __init__(self, row, col, graph):
        self._row = row
        self._col = col
        self._graph = graph
        self._count = 0

        self._visited = [
            [
                False for _ in range(self.col)
            ]
            for _ in range(self.row)
        ]

        self._islands = [
            [
                0 for _ in range(self.col)
            ]
            for _ in range(self.row)
        ]

    @property
    def row(self):
        return self._row

    @property
    def col(self):
        return self._col

    @property
    def graph(self):
        return self._graph

    @property
    def visited(self):
        return self._visited

    @property
    def islands(self):
        return self._islands

    @property
    def count(self):
        return self._count

    @row.setter
    def row(self, value):
        if not isinstance(value, int):
            raise TypeError("row should be from type int")

        self._row = value

    @col.setter
    def col(self, value):
        if not isinstance(value, int):
            raise TypeError("col should be from type int")

        self._col = value

    @count.setter
    def count(self, value):
        if not isinstance(value, int):
            raise TypeError("count should be from type int")

        self._count = value

    @graph.setter
    def graph(self, value):
        if not isinstance(value, list):
            raise TypeError("graph should be from type list")

        for i in value:
            if not isinstance(i, list):
                raise TypeError("graph items should be from type list")

        self._graph = value

    def is_safe(self, i, j):
        # row number is in range, column number
        # is in range and value is 1 
        # and not yet visited
        return 0 <= i < self.row \
               and 0 <= j < self.col \
               and not self.visited[i][j] \
               and self.graph[i][j]

    # A utility function to do DFS for a 2D
    # boolean matrix. It only considers
    # the 8 neighbours as adjacent vertices
    def dfs(self, i, j, island_number):

        # These arrays are used to get row and
        # column numbers of 8 neighbours
        # of a given cell
        row_nbr = [-1, -1, -1, 0, 0, 1, 1, 1]
        col_nbr = [-1, 0, 1, -1, 1, -1, 0, 1]

        # Mark this cell as visited
        self.visited[i][j] = True
        self.islands[i][j] = island_number

        # Recur for all connected neighbours
        for k in range(len(row_nbr)):
            print("I am i:{:d} j:{:d}".format(i, j))
            if self.is_safe(i + row_nbr[k], j + col_nbr[k]):
                print("visiting i:{:d}, j:{:d}".format(i + row_nbr[k], j + col_nbr[k]))
                self.dfs(i + row_nbr[k], j + col_nbr[k], island_number)
            else:
                print("not VALID i:{:d}, j:{:d}".format(i + row_nbr[k], j + col_nbr[k]))

    # The main function that returns
    # count of islands in a given boolean
    # 2D matrix
    def count_islands(self):
        # Initialize count as 0 and travese
        # through the all cells of
        # given matrix
        for i in range(self.row):
            for j in range(self.col):
                # If a cell with value 1 is not visited yet,
                # then new island found
                if not self.visited[i][j] and self.graph[i][j] == 1:
                    # Visit all cells in this island
                    # and increment island count
                    self.dfs(i=i, j=j, island_number=self.count+1)
                    self.count += 1

        return self.count

    def print_graph(self):
        for i in self.graph:
            print(i)
        print("##############")
        for i in self.islands:
            print(i)


graph = [[1, 1, 0, 0, 0],
         [0, 1, 0, 0, 1],
         [1, 0, 0, 1, 1],
         [0, 0, 0, 0, 0],
         [1, 0, 1, 0, 1]]

row = len(graph)
col = len(graph[0])

g = Graph(row, col, graph)
print(graph)
print(g.count_islands())
print(g.print_graph())
