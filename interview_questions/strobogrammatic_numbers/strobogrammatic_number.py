#!/usr/bin/env python3

shadow_map = {
    "0": "0",
    "1": "1",
    "6": "9",
    "8": "8",
    "9": "6",
}


def if_stobogramic(number: str) -> bool:
    for index, num in enumerate(number):
        strobo = shadow_map.get(num)
        if not strobo or number[-(index+1)] != strobo:
            print("{} is not stobogramic".format(number))
            return False

    print("{} is stobogramic".format(number))
    return True


# [if_stobogramic(i) for i in ["00", "112", "69", "96", "88", "118", "18181", "669"]]


def gen_strobogrammatic_nubmers(n):
    result = generate_numbers(n)
    return result


def generate_numbers(n):
    if n == 0:
        return [""]

    if n == 1:
        return ["1", "0", "8"]

    middles = generate_numbers(n - 2)
    print("middles: {}".format(", ".join(middles)))
    result = []

    for middle in middles:
        result.append("0" + middle + "0")
        result.append("1" + middle + "1")
        result.append("6" + middle + "9")
        result.append("8" + middle + "8")
        result.append("9" + middle + "6")
    return result


[print(sorted(gen_strobogrammatic_nubmers(i))) for i in range(1, 4)]
