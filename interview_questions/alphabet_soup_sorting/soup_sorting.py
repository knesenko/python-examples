#!/usr/bin/env python


from typing import List

alphabet_ranks = {}


def _init_alphabet(alphabet: List) -> None:
    for index, letter in enumerate(alphabet):
        alphabet_ranks[letter] = index


def _is_sorted(first_word: str, second_word: str) -> bool:
    for i in range(min(len(first_word), len(second_word))):
        if first_word[i] != second_word[i]:
            return alphabet_ranks[first_word[i]] < alphabet_ranks[second_word[i]]

    return len(first_word) <= len(second_word)


def is_alphabet_sorted(words: List, alphabet: List) -> bool:
    _init_alphabet(alphabet)
    for i in range(len(words) - 1):
        first_word = words[i]
        second_word = words[i+1]

        if not _is_sorted(first_word, second_word):
            return False

    return True


print(is_alphabet_sorted(["cc", "cb", "bc", "ac"], ['c', 'b', 'a']))
print(is_alphabet_sorted(["cc", "cb", "bc", "ac"], ['b', 'c', 'a']))
