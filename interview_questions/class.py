

class A(object):
    def __init__(self):
        self.a = "a"

    def call_me_a(self):
        print(self.a)

class B(A):
    def __init__(self):
        super(B, self).__init__()


if __name__ == "__main__":
    b = B()
    b.call_me_a()
