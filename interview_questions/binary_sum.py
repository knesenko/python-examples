

"""
1 + 1 + 1 = 3 = 1,1
1 + 1 + 0 = 2 = 0,1
0 + 0 + 1 = 1 = 1,0
0 + 0 + 0 = 0 = 0,0

"""


def binary_sum(first, second):
    result = ""
    remember = 0

    if len(first) > len(second):
        first_string = first
        second_string = second
    else:
        first_string = second
        second_string = first

    for i in reversed(range(len(first_string), 0, -1)):
        first_string_c = int(first_string[-i])
        try:
            second_string_c = int(second_string[-i])
        except IndexError:
            second_string_c = 0

        sum = remember + first_string_c + second_string_c
        if sum == 3:
            remember = 1
            answer = 1
        elif sum == 2:
            remember = 1
            answer = 0
        elif sum == 1:
            remember = 0
            answer = 1
        elif sum == 0:
            remember = 0
            answer = 0

        result = str(answer) + str(result)

    return result if remember == 0 else str(remember) + str(result)


for i in [["101", "1"], ["10", "1"], ["111", "10"]]:
    print(binary_sum(i[0], i[1]))

