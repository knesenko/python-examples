commits = [
    (0, True),
    (1, True),
    (2, True),
    (3, False),
    (4, False),
    (5, False),
    (6, False),
    (7, False),
    (8, False),
    (9, False),
    (10, False),
]


def is_bad(commit):
    return not commits[commit][1]


def find_bad_commit(good, bad):
    mid = -1
    while good < bad:
        if (good + bad) // 2 == mid:
            return commits[bad][0]

        mid = (good + bad) // 2
        if is_bad(mid):
            bad = mid
        else:
            good = mid


print(find_bad_commit(0, 10))


