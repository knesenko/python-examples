ARRAY = [6, 2, 3, 4, 7, 5, 8]
TARGET_SUM = 19


def triple_sum(array, target_sum):
    array = sorted(array)
    for i in range(len(array)):
        left = i + 1
        right = len(array) - 1
        while left < right:
            computed_sum = array[i] + array[right] + array[left]
            if computed_sum == target_sum:
                print("{}+{}+{}={}".format(
                    str(array[i]),
                    str(array[right]),
                    str(array[left]),
                    str(target_sum),
                ))
                return True
            elif right - left == 1:
                i += 1
            elif computed_sum < target_sum:
                left += 1
            else:
                right -= 1

        return False


print(triple_sum(array=ARRAY, target_sum=TARGET_SUM))
