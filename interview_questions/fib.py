import sys


def fib(n):
    if n == 0:
        return 0
    if n == 1:
        return 1
    else:
        return fib(n - 1) + fib(n - 2)


def F():
    a, b = 0, 1
    while True:
        yield a
        a, b = b, a + b


# for i in range(int(sys.argv[1])):
#     print(fib(i))

for i in F():
    if i != 13:
        print(i)
    else:
        break

