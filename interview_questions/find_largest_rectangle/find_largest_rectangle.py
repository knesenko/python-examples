

class Solution(object):
    def largestRectangleArea(self, histogram):
        """
        :type histogram: List[int]
        :rtype: int
        """
        stack = []
        max_area = -1
        n = len(histogram)
        i = 0

        while i < n:
            if not stack or histogram[stack[-1]] <= histogram[i]:
                stack.append(i)
                i += 1
            else:
                top = stack.pop()
                if not stack:
                    area = histogram[top] * i
                else:
                    area = histogram[top] * (i - stack[-1] - 1)

                max_area = max(area, max_area)

        while stack:
            top = stack.pop()
            if not stack:
                area = histogram[top] * i
            else:
                area = histogram[top] * (i - stack[-1] - 1)

            max_area = max(area, max_area)

        return max_area

    def largest_RectangleArea(self, A):
        ans = 0
        A = [-1] + A
        A.append(-1)
        n = len(A)
        stack = [0]  # store index
        print(A)

        for i in range(n):
            while A[i] < A[stack[-1]]:
                h = A[stack.pop()]
                area = h * (i - stack[-1] - 1)
                ans = max(ans, area)
            stack.append(i)
        return ans


if __name__ == "__main__":
    print(Solution().largestRectangleArea([6, 2, 5, 4, 5, 1, 6]))
    # print(Solution().largest_RectangleArea([2, 1, 5, 6, 2, 3]))
